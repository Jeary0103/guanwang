<?php

/**
 * 后台路由组
 */
Route::group(['middleware'=>'admin.route','prefix'=>'admin','namespace'=>'Admin','as'=>'admin.'],function (){
    Route::get('/','IndexController@index')->name('index');
    // 栏目资源路由
    Route::resource('category','CategoryController');
    // 文章资源路由（新闻）
    Route::resource('article','ArticleController');
    // 轮播图资源路由
    Route::resource('slide','SlideController');
    // 关于我们资源路由
    Route::resource('about','AboutController');

});

// 入口路由
Route::get('/',function (){
    return redirect('/home/index');
});

/**
 *  前后路由组
 */

Route::group(['prefix'=>'home','namespace'=>'Home','as'=>'home.'],function (){

    // 网站首页
    Route::get('index','IndexController@index')->name('index');

    // 关于我们
    Route::get('about','IndexController@about')->name('about');

    // 新闻列表
    Route::get('news','IndexController@news')->name('news');

    // 新闻详情页
    Route::get('news/{article}','IndexController@news_detail')->name('news_detail');

    // 案例展示
    Route::get('case_list','IndexController@case_list')->name('case_list');

    // 案例详情
    Route::get('case_detail/{article}','IndexController@case_detail')->name('case_detail');

    //加盟我们
    Route::get('service','IndexController@service')->name('service');

    //游戏专题
    Route::get('games','IndexController@games')->name('games');

    //联系我们
    Route::get('links','IndexController@links')->name('links');
});

/**
 *  公共路由组
 */
Route::group( ['prefix' => 'common', 'namespace' => 'Common', 'as' => 'common.'],function (){
    Route::any('uploader/make', 'UploaderController@make')->name('uploader.make');
    Route::any('uploader/lists', 'UploaderController@lists')->name('uploader.lists');
});

/**
 * 用户登录路由
 */
Route::group(['prefix'=>'user','namespace'=> 'User','as'=>'user.'],function (){
    Route::any('login','LoginController@login')->name('login');
    Route::post('store','LoginController@store')->name('store');
    Route::get('logout','LoginController@logout')->name('logout');
});
