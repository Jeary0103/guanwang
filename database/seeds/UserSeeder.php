<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 给用户表里面填充两条用户数据
        $users = [
            [
                'name' => 'wangzhiyuan',
                'email' => '237618121@qq.com',
                'password' => bcrypt('admin888')
            ],
            [
                'name' => 'liqiaomei',
                'email' => '123456@qq.com',
                'password' => bcrypt('admin888')
            ]
        ];
        // 通过create方法只能是一条一条的往里面添加数据的
        foreach($users as $user){
           \App\User::create($user);
        }
        // 注意：这里写完以后，要到DatabaseSeeder.php里面去注册一下才可以的。
    }
}
