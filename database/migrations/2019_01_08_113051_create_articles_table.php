<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('文章标题');
            $table->string('icon')->comment('阿里图标');
            $table->string('source')->nullable()->comment('来源');
            $table->string('author')->nullable()->comment('作者');
            $table->string('description')->nullable()->comment('文章简介');
            $table->string('thumb')->nullable()->comment('缩略图');
            $table->unsignedInteger('category_id')->index()->comment('栏目');
            $table->unsignedInteger('user_id')->index()->nullable()->comment('发布文章用户编号'); 
            $table->text('content')->comment('内容');
            $table->string('redirect_url')->nullable()->comment('文章跳转链接');
            $table->string('click')->default(0)->comment('查看次数');
            $table->unsignedInteger('is_top')->comment('是否置顶');
            $table->mediumText('fields')->nullable()->comment('扩展字段');
            $table->string('article_template')->nullable()->comment('文章模板');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
