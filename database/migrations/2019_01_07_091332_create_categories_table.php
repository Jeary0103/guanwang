<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->comment('栏目名称');
            $table->unsignedInteger('parent_id')->default(0)->comment('父级栏目');
            $table->string('description')->nullable()->comment('栏目描述');
            $table->unsignedInteger('is_home')->default(0)->comment('封面栏目');
            $table->string('home_template')->default('home')->comment('栏目封面模板');
            $table->string('list_template')->default('list')->comment('列表页模板');
            $table->string('content_template')->default('content')->comment('内容页模板');
            $table->unsignedInteger('content_model_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
