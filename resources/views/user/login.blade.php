<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{asset('org/admin')}}/fonts/feather/feather.min.css">
    <link rel="stylesheet" href="{{asset('org/admin')}}/libs/highlight/styles/vs2015.min.css">
    <link rel="stylesheet" href="{{asset('org/admin')}}/libs/quill/dist/quill.core.css">
    <link rel="stylesheet" href="{{asset('org/admin')}}/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('org/admin')}}/libs/flatpickr/dist/flatpickr.min.css">

    <link href="{{asset('org/admin')}}/css/theme-dark.min.css" rel="" data-toggle="theme"
          data-theme-mode="dark">
    <link href="{{asset('org/admin')}}/css/theme.min.css" rel="" data-toggle="theme" data-theme-mode="light">

    <style>
        body {
            display: none;
        }
    </style>
    <script src="{{asset('org/admin/js/sweetalert.min.js')}}"></script>
    <script>
        var themeMode = (localStorage.getItem('dashkitThemeMode')) ? localStorage.getItem('dashkitThemeMode') : 'light';
        var themeFile = document.querySelector('[data-toggle="theme"][data-theme-mode="' + themeMode + '"]');

        // Enable stylesheet
        themeFile.rel = 'stylesheet';

        // Enable body content
        themeFile.addEventListener('load', function () {
            document.body.style.display = 'block';
        });
    </script>
    <title>后台登录系统</title>
    @include('layouts._hdjs')
</head>
<body class="d-flex align-items-center bg-auth border-top border-top-2 border-primary">
@include('layouts._message')
<div class="container-fluid">
    <div class="row align-items-center justify-content-center">
        <div class="col-12 col-md-5 col-lg-6 col-xl-4 px-lg-6 my-5">
            <!-- Heading -->
            <h1 class="display-4 text-center mb-3">双猴管理系统</h1>
            <!-- Subheading -->
            <p class="text-muted text-center mb-5">欢迎使用本系统！</p>
            <!-- Form -->
            <form method="post" action="{{route('user.store')}}">
            @csrf
            <!-- Email address -->
                <div class="form-group">
                    <label>邮箱</label>
                    <input type="email" name="email" class="form-control">
                </div>
                <!-- Password -->
                <div class="form-group">
                    <label>密码</label>
                    <div class="input-group input-group-merge">
                        <input type="password" name="password" class="form-control form-control-appended">
                        <div class="input-group-append">
                          <span class="input-group-text">
                            <i class="fe fe-eye"></i>
                          </span>
                        </div>
                    </div>
                </div>
                <button class="btn btn-lg btn-block btn-primary mb-3">登录</button>

                {{--<div class="text-center">--}}
                    {{--<small class="text-muted text-center">--}}
                        {{--还没有网站帐号? <a href="{{route('register')}}">注册</a>--}}
                        {{--<a href="{{route('findPassword')}}">找回密码</a>--}}
                    {{--</small>--}}
                {{--</div>--}}
            </form>
        </div>
        <div class="col-12 col-md-7 col-lg-6 col-xl-8 d-none d-lg-block">
            <!-- Image -->
            <div class="bg-cover vh-100 mt--1 mr--3"
                 style="background-image: url({{asset('org/admin')}}/img/covers/auth-side-cover.jpg);"></div>
        </div>
    </div>
</div>
</body>
</html>