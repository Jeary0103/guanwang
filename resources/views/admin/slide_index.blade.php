@extends('admin.layouts.master')

@section('content')
    <div class="card mt-2">
        {{--头部tab按钮--}}
        <div class="card-header">
            <div class="col">
                <ul class="nav nav-tabs nav-overflow header-tabs">
                    <li class="nav-item">
                        <a href="{{route('admin.slide.index')}}" class="nav-link active" style="padding-bottom: 9px;">
                            幻灯片列表
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.slide.create')}}" class="nav-link "
                        >
                            添加幻灯片
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <form action="{{route('admin.slide.store')}}" method="post">
            @csrf
            {{--表单部分--}}
            <div class="card-body">
                <table class="table text-center align">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>幻灯片描述</th>
                        <th>跳转链接</th>
                        <th>图片</th>
                        <th>创建时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($slides as $slide)
                        <tr>
                            <td scope="row">{{$slide['id']}}</td>
                            <td>{{$slide['title']}}</td>
                            <td>
                                <a href="{{$slide['url']}}" target="_blank">{{$slide['url']}}</a>
                            </td>
                            <td>
                                <div class="avatar avatar-lg avatar-4by3">
                                    <a href="{{$slide['image']}}" target="_blank">
                                        <img src="{{$slide['image']}}" alt="..." class="avatar-img rounded"
                                        style="width:50px; height:50px;">
                                    </a>
                                </div>
                            </td>
                            <td>{{$slide->created_at}}</td>
                            <td class="text-right">
                                <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                    <a href="{{route('admin.slide.edit',$slide)}}" class="btn btn-light">编辑幻灯片</a>
                                    <button type="button" class="btn btn-white" onclick="del(this)">删除幻灯片</button>
                                    <form action="{{route('admin.slide.destroy',$slide)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script>
        function del(obj) {
            if(confirm('确认删除吗?')){
                $(obj).next().submit();
            }
        }
    </script>
@endpush