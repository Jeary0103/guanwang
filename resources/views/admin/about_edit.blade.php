@extends('admin.layouts.master')
@section('content')
    <div class="card mt-2">
        {{--头部tab按钮--}}
        <div class="card-header">
            <div class="col">
                <ul class="nav nav-tabs nav-overflow header-tabs">
                    <li class="nav-item">
                        <a href="{{route('admin.about.index')}}" class="nav-link">
                            关于列表
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.about.edit',$about)}}" class="nav-link active"
                           style="padding-bottom: 9px;">
                            编辑关于
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <form action="{{route('admin.about.update',$about)}}" method="post">
            @csrf
            @method('PUT')
            {{--表单部分--}}
            <div class="card-body">
                @include('admin.layouts._about')
            </div>
            {{--提交按钮--}}
            <div class="card-footer text-muted">
                <button class="btn btn-primary btn-sm">保存提交</button>
            </div>
        </form>
    </div>
@endsection