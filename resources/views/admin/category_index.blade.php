@extends('admin.layouts.master')

@section('content')
    <div class="card mt-2">
        <div class="card-header">
            <div class="col">
                <ul class="nav nav-tabs nav-overflow header-tabs">
                    <li class="nav-item">
                        <a href="{{route('admin.category.index')}}" class="nav-link active" style="padding-bottom:9px;">
                            栏目列表
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.category.create')}}" class="nav-link">
                            添加栏目
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <table class="table table-{1:striped|sm|bordered|hover|inverse} table-inverse text-center">
                    <thead class="thead-inverse|thead-default">
                    <tr>
                        <th>编号</th>
                        <th>分类名称</th>
                        <th>父级分类名</th>
                        <th>封面栏目</th>
                        <th>创建时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td scope="row">{{$category['id']}}</td>
                            <td>{{$category['title']}}</td>
                            <td>
                                @if($category['parent_id'] == 0)
                                    顶级栏目
                                @else
                                    {{$category['parent_category']['title']}}
                                @endif
                            </td>
                            <td>
                                @if($category['is_home']==1)
                                    <span class="fe fe-check-circle text-info"></span>
                                @else
                                    <span class="fe fe-x-circle text-secondary"></span>
                                @endif
                            </td>
                            <td>{{$category->created_at->diffForHumans()}}</td>
                            <td>
                                <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                    <a href="{{route('admin.category.edit',$category)}}" class="btn btn-light">编辑分类</a>
                                    <button class="btn btn-white" onclick="del(this)">删除分类</button>
                                    <form type="button" action="{{route('admin.category.destroy',$category)}}"
                                          method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer text-muted">
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    {{$categories->links()}}
                </ul>
            </nav>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function del(m) {
            if(confirm('确认删除吗？')){
                $(m).next().submit();
            }
        }
    </script>
@endpush