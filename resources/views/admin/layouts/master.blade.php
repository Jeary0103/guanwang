<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="双猴科技官网,双猴科技是一家集互联网软件开发,游戏开发,商城,小程序开发的大型软件公司。">
    {{--令牌--}}
    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{asset('org/admin')}}/fonts/feather/feather.min.css">
    <link rel="stylesheet" href="{{asset('org/admin')}}/libs/highlight/styles/vs2015.min.css">
    <link rel="stylesheet" href="{{asset('org/admin')}}/libs/quill/dist/quill.core.css">
    <link rel="stylesheet" href="{{asset('org/admin')}}/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('org/admin')}}/libs/flatpickr/dist/flatpickr.min.css">
    {{--css占位--}}
    @stack('css')
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('org/admin')}}/css/theme.min.css">
    {{--弹框 sweetalert 弹出层样式文件--}}
    <script src="{{asset('org/admin/js/sweetalert.min.js')}}"></script>
    {{--引入 hdjs--}}
    @include('layouts._hdjs')
    <title>双猴科技官网</title>
</head>
<body>
{{-- swal 弹框 session错误代码片段 --}}
@include('layouts._message')

<!-- 左侧导航 -->
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebarCollapse"
                aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- logo设置 -->
        <a class="navbar-brand" href="javascript:;">
            <img src="{{asset('org/admin')}}/img/logo.svg" class="navbar-brand-img
          mx-auto" alt="...">
        </a>
        <!-- User (xs) -->
        <div class="navbar-user d-md-none">
            <!-- Dropdown -->
            <div class="dropdown">
                <!-- Toggle -->
                <a href="#!" id="sidebarIcon" class="dropdown-toggle" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <div class="avatar avatar-sm avatar-online">
                        <img src="{{asset('org/admin')}}/img/avatars/profiles/avatar-1.jpg"
                             class="avatar-img rounded-circle" alt="...">
                    </div>
                </a>

                <!-- Menu -->
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sidebarIcon">
                    {{--<a href="profile-posts.html" class="dropdown-item">Profile</a>--}}
                    {{--<a href="settings.html" class="dropdown-item">Settings</a>--}}
                    <hr class="dropdown-divider">
                    <a href="#" class="dropdown-item">退出登录</a>
                </div>

            </div>

        </div>

        <!-- 左侧导航栏设置 -->
        <div class="collapse navbar-collapse" id="sidebarCollapse">

            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended"
                           placeholder="Search" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fe fe-search"></span>
                        </div>
                    </div>
                </div>
            </form>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.index')}}">
                        <i class="fe fe-home"></i> 后台首页
                    </a>
                </li>
                {{--栏目管理 --}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarPages" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarPages">
                        <span class="fe fe-grid"></span>栏目管理
                    </a>
                    <div class="collapse " id="sidebarPages">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{route('admin.category.index')}}" class="nav-link" role="button"
                                   aria-expanded="false" aria-controls="sidebarProfile">
                                    栏目列表
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.category.create')}}" class="nav-link" role="button"
                                   aria-expanded="false" aria-controls="sidebarProfile">
                                    添加栏目
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                {{--新闻管理--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarPages2" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarPages">
                        <span class="fe fe-monitor"></span> 新闻动态
                    </a>
                    <div class="collapse " id="sidebarPages2">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{route('admin.article.index')}}" class="nav-link" role="button"
                                   aria-expanded="false" aria-controls="sidebarProfile">
                                    新闻列表
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.article.create')}}" class="nav-link" role="button"
                                   aria-expanded="false" aria-controls="sidebarProfile">
                                    添加新闻
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                {{--关于我们--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarPages8" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarPages">
                        <span class="fe fe-user"></span> 关于我们
                    </a>
                    <div class="collapse " id="sidebarPages8">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{route('admin.about.index')}}" class="nav-link" role="button" aria-expanded="false"
                                   aria-controls="sidebarProfile">
                                    关于列表
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.about.create')}}" class="nav-link" role="button" aria-expanded="false"
                                   aria-controls="sidebarProfile">
                                    添加关于
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                {{--轮播管理--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarPages5" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarPages">
                        <span class="fe fe-film"></span> 幻灯片管理
                    </a>
                    <div class="collapse " id="sidebarPages5">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{route('admin.slide.index')}}" class="nav-link" role="button" aria-expanded="false"
                                   aria-controls="sidebarProfile">
                                    轮播图列表
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.slide.create')}}" class="nav-link" role="button" aria-expanded="false"
                                   aria-controls="sidebarProfile">
                                    添加轮播图
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                {{--用户和角色--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarPages4" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarPages">
                        <span class="fe fe-users"></span> 用户和角色
                    </a>
                    <div class="collapse " id="sidebarPages4">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="#sidebarProfile4" class="nav-link" role="button" aria-expanded="false"
                                   aria-controls="sidebarProfile">
                                    用户列表
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#sidebarProfile4" class="nav-link" role="button" aria-expanded="false"
                                   aria-controls="sidebarProfile">
                                    角色列表
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                {{--后台管理--}}
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarPages3" data-toggle="collapse" role="button" aria-expanded="false"
                       aria-controls="sidebarPages">
                        <span class="fe fe-settings"></span> 后台配置项管理
                    </a>
                    <div class="collapse " id="sidebarPages3">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="#sidebarProfile3" class="nav-link" role="button" aria-expanded="false"
                                   aria-controls="sidebarProfile">
                                    权限列表
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#sidebarProfile3" class="nav-link" role="button" aria-expanded="false"
                                   aria-controls="sidebarProfile">
                                    配置项
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
            </ul>

            <!-- Divider -->
            <hr class="my-3">
        </div>

    </div>
</nav>
{{--左侧导航结束--}}

<!-- 右侧内容部分 包含顶部和主题两部分-->
<div class="main-content">
    <!-- 顶部导航 -->
    <nav class="navbar navbar-expand-md navbar-light bg-white d-none d-md-flex">
        <div class="container-fluid">

            <!-- 标题 -->
            <a class="navbar-brand mr-auto" href="javascript:;">
                <span class="btn btn-primary">双猴官网后台管理系统 </span>
            </a>

            <!-- 搜索框开始 -->
            <form class="form-inline mr-4 d-none d-md-flex">
                <div class="input-group input-group-rounded input-group-merge" data-toggle="lists"
                     data-lists-values='["name"]'>
                    <!-- Input -->
                    <input type="search" class="form-control form-control-prepended  dropdown-toggle search"
                           data-toggle="dropdown" placeholder="Search" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fe fe-search"></i>
                        </div>
                    </div>
                    <!-- Menu -->
                    <div class="dropdown-menu dropdown-menu-card">
                        <div class="card-body">
                            <!-- List group -->
                            <div class="list-group list-group-flush list my--3">
                                <a href="javascript:;" class="list-group-item px-0">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <!-- Avatar -->
                                            <div class="avatar">
                                                <img src="{{asset('org/admin')}}/img/avatars/teams/team-logo-1.jpg"
                                                     alt="..." class="avatar-img rounded">
                                            </div>

                                        </div>
                                        <div class="col ml--2">
                                            <!-- Title -->
                                            <h4 class="text-body mb-1 name">
                                                Airbnb
                                            </h4>

                                            <!-- Time -->
                                            <p class="small text-muted mb-0">
                                                <span class="fe fe-clock"></span>
                                                <time datetime="2018-05-24">Updated 2hr ago</time>
                                            </p>

                                        </div>
                                    </div> <!-- / .row -->
                                </a>
                            </div>
                        </div>
                    </div> <!-- / .dropdown-menu -->

                </div>
            </form>
        {{--搜索框结束--}}

        <!-- User -->
            <div class="navbar-user">
                <!-- Dropdown -->
                <div class="dropdown mr-4 d-none d-md-flex">
                    <!-- Toggle -->
                    <a href="#" class="text-muted" role="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <span class="icon active">
                            <i class="fe fe-bell"></i>
                         </span>
                    </a>
                    <!-- 通知 -->
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">

                                    <!-- Title -->
                                    <h5 class="card-header-title">
                                        通知
                                    </h5>

                                </div>
                                <div class="col-auto">
                                    <!-- Link -->
                                    <a href="#!" class="small">
                                        View all
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="list-group list-group-flush my--3">
                                <a class="list-group-item px-0" href="#!">

                                    <div class="row">
                                        <div class="col-auto">
                                            <!-- Avatar -->
                                            <div class="avatar avatar-sm">
                                                <img src="{{asset('org/admin')}}/img/avatars/profiles/avatar-1.jpg"
                                                     alt="..." class="avatar-img rounded-circle">
                                            </div>

                                        </div>
                                        <div class="col ml--2">

                                            <!-- Content -->
                                            <div class="small text-muted">
                                                <strong class="text-body">Dianna Smiley</strong> shared your post with
                                                <strong class="text-body">Ab Hadley</strong>, <strong class="text-body">Adolfo
                                                    Hess</strong>, and <strong class="text-body">3 others</strong>.
                                            </div>

                                        </div>
                                        <div class="col-auto">

                                            <small class="text-muted">
                                                2m
                                            </small>

                                        </div>
                                    </div> <!-- / .row -->

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Dropdown -->
                <div class="dropdown">
                    <!-- Toggle -->
                    <a href="#" class="avatar avatar-sm avatar-online dropdown-toggle" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{asset('org/admin')}}/img/avatars/profiles/avatar-1.jpg" alt="..."
                             class="avatar-img rounded-circle">
                    </a>
                    <!-- Menu -->
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item">Profile</a>
                        <a href="javascript:;" class="dropdown-item">Settings</a>
                        <hr class="dropdown-divider">
                        <a href="{{route('user.logout')}}" class="dropdown-item">退出登录</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    {{--顶部导航结束--}}

    {{--右侧 内容部分  以占位的形式表现的--}}
    <div class="container">
        @yield('content')
    </div>
    {{--内容部分结束--}}

</div>

<!-- JAVASCRIPT -->

<!-- Libs JS -->
{{--<script src="{{asset('org/admin')}}/libs/jquery/dist/jquery.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/chart.js/dist/Chart.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/chart.js/Chart.extension.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/highlight/highlight.pack.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/flatpickr/dist/flatpickr.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/jquery-mask-plugin/dist/jquery.mask.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/list.js/dist/list.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/quill/dist/quill.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/dropzone/dist/min/dropzone.min.js"></script>--}}
{{--<script src="{{asset('org/admin')}}/libs/select2/dist/js/select2.min.js"></script>--}}

{{--<!-- Theme JS -->--}}
{{--<script src="{{asset('org/admin')}}/js/theme.min.js"></script>--}}

@stack('js')
</body>
</html>