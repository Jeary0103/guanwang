{{--描述--}}
<div class="form-group">
    <label for="">请输入幻灯片描述</label>
    <input type="text"  name="title" value="{{old('title',$slide['title']??'')}}" class="form-control" placeholder="请输入幻灯片描述">
</div>
{{--图片上传--}}
<div class="form-group">
    <label for="">缩略图</label>

    <div class="input-group mb-2">
        <input class="form-control"  name="image" readonly="" value="{{old('image',$slide['image']??'')}}">
        <div class="input-group-append">
            <button onclick="upImagePc(this)" class="btn btn-secondary" type="button">单图上传</button>
        </div>
    </div>
    <div style="display: inline-block;position: relative;">
        <img src="/images/nopic.jpg" class="img-responsive img-thumbnail" width="100">
        <em class="close" style="position: absolute;top: 3px;right: 8px;" title="删除这张图片"
            onclick="removeImg(this)">×</em>
    </div>

    <script>
        require(['hdjs','bootstrap']);
        //上传图片
        function upImagePc() {
            require(['hdjs'], function (hdjs) {
                var options = {
                    multiple: false,//是否允许多图上传
                    //data是向后台服务器提交的POST数据
                    data: {name: '后盾人', year: 2099},
                };
                hdjs.image(function (images) {
                    //上传成功的图片，数组类型
                    $("[name='image']").val(images[0]);
                    $(".img-thumbnail").attr('src', images[0]);
                }, options)
            });
        }
        //移除图片
        function removeImg(obj) {
            // 当删除图片时，应该展示一张默认的图标来占位，这样会表现好一点
            $(obj).prev('img').attr('src', "{{asset('images/nopic.jpg')}}");
            // 当移除图片的同时，应该把表单里面的值也同时清空
            $(obj).parent().prev().find('input').val('');
        }
    </script>
</div>
{{--跳转地址--}}
<div class="form-group">
    <label for="">请输入链接地址</label>
    <input type="text"  name="url" value="{{old('url',$slide['url']??'')}}" class="form-control" placeholder="请输入跳转地址" value="http://">
</div>