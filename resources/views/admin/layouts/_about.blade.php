<div class="card">
    <div class="card-body">
        {{--文章标题--}}
        <div class="form-group">
            <label>标题</label>
            <input type="text" name="title" value="{{old('title',$about['title']??'')}}" class="form-control">
        </div>
        {{--所属栏目--}}
        <div class="form-group">
            <label>所属栏目</label>
            <select name="category_id" class="form-control">
                <option value="0">顶级栏目</option>
                {{--这里判断，如果是编辑状态，说明存在$about 这个变量，就处理选中状态值--}}
                {{--如果 不存在 $about 说明是添加新闻，就不用去判断 分类的选中状态了--}}
                @if(isset($about))
                    @foreach($categories as $cat)
                        <option value="{{$cat['id']}}" {{($cat['id'] == $about->category->id)?'selected':''}}>
                            {!! $cat['_title'] !!}
                        </option>
                    @endforeach
                @else
                    @foreach($categories as $cat)
                        <option value="{{$cat['id']}}" }>
                            {!! $cat['_title'] !!}
                        </option>
                    @endforeach
                @endif
            </select>
        </div>
        {{--缩略图--}}
        <div class="form-group">
            <label>内容图</label>
            <div class="input-group mb-2">
                <input class="form-control" name="thumb" value="{{old('thumb',$about['thumb']??'')}}">
                <div class="input-group-append">
                    <button onclick="upImagePc(this)" class="btn btn-outline-secondary" type="button">单图上传</button>
                </div>
            </div>
            <div style="display: inline-block;position: relative;">
                <img src="{{asset('images/nopic.jpg')}}" class="img-responsive img-thumbnail" width="100">
                <em class="close" style="position: absolute;top: 3px;right: 8px;" title="删除这张图片"
                    onclick="removeImg(this)">×</em>
            </div>
            <script>
                require(['hdjs', 'bootstrap']);

                //上传图片
                function upImagePc() {
                    require(['hdjs'], function (hdjs) {
                        var options = {
                            multiple: false,//是否允许多图上传
                            //data是向后台服务器提交的POST数据
                            data: {name: '后盾人', year: 2099},
                        };
                        hdjs.image(function (images) {
                            // images 是一个成功回调值
                            // console.log(images);
                            //上传成功的图片，数组类型
                            $("[name='thumb']").val(images[0]);
                            $(".img-thumbnail").attr('src', images[0]);
                        }, options)
                    });
                }

                //移除图片
                function removeImg(obj) {
                    // 当删除图片时，应该展示一张默认的图标来占位，这样会表现好一点
                    $(obj).prev('img').attr('src', "{{asset('images/nopic.jpg')}}");
                    // 当移除图片的同时，应该把表单里面的值也同时清空
                    $(obj).parent().prev().find('input').val('');
                }
            </script>
        </div>
        {{--百度编辑器--}}
        <div class="form-group">
            <label>内容</label>
            <textarea id="container" style="height:300px;width:100%;"
                      name="content">{{old('content',$about['content']??'')}}</textarea>
            @push('js')
                <script>
                    require(['hdjs'], function (hdjs) {
                        hdjs.ueditor('container');
                    })
                </script>
            @endpush
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        扩展字段
    </div>
</div>