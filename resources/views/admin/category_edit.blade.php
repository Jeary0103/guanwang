@extends('admin.layouts.master')
@section('content')
    <div class="card mt-2">
        {{--头部tab按钮--}}
        <div class="card-header">
            <div class="col">
                <ul class="nav nav-tabs nav-overflow header-tabs">
                    <li class="nav-item">
                        <a href="{{route('admin.category.index')}}" class="nav-link">
                            栏目列表
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.category.edit',$category)}}" class="nav-link active"
                           style="padding-bottom: 9px;">
                            编辑栏目
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <form action="{{route('admin.category.update',$category)}}" method="post">
            @csrf
            @method('PUT')
            {{--表单部分--}}
            <div class="card-body">
                <div class="card">
                    <div class="card-header">
                        基本设置
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>栏目名称</label>
                            <input type="text" name="title" class="form-control" placeholder="请输入栏目名称"
                                   value="{{$category['title']}}" aria-describedby="helpId">
                        </div>
                        <div class="form-group">
                            <label>父级栏目</label>
                            <select name="parent_id" class="form-control">
                                <option value="0">顶级栏目</option>
                                @foreach($categories as $cat)
                                    <option value="{{$cat['id']}}"
                                            {{$cat['is_parent']?'selected':''}}
                                            {{$cat['id'] == $category['id'] || $cat['is_child'] ? 'disabled' : ''}}>
                                        {!! $cat['_title'] !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>栏目描述</label>
                            <textarea name="description" class="form-control" rows="3" placeholder="请输入栏目描述"></textarea>
                        </div>
                        <div class="form-group">
                            <label>封面栏目</label>
                            <div class="custom-control custom-checkbox-toggle">
                                <input type="checkbox" class="custom-control-input" name="is_home" value="1"
                                       id="is_home" {{$category['is_home']?'checked':''}}>
                                <label class="custom-control-label" for="is_home"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        模板设置
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>封面模板</label>
                            <input type="text" name="home_template" class="form-control"
                                   value="{{old('home_template','home')}}">
                        </div>
                        <div class="form-group">
                            <label>列表页模板</label>
                            <input type="text" name="list_template" class="form-control"
                                   value="{{old('home_template','list')}}">
                        </div>
                        <div class="form-group">
                            <label>文章内容页模板</label>
                            <input type="text" name="content_template" class="form-control"
                                   value="{{old('home_template','content')}}">
                        </div>
                    </div>
                </div>
            </div>
            {{--提交按钮--}}
            <div class="card-footer text-muted">
                <button class="btn btn-primary btn-sm">保存提交</button>
            </div>
        </form>
    </div>
@endsection