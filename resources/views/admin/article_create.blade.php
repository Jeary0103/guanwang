@extends('admin.layouts.master')
@section('content')
    <div class="card mt-2">
        {{--头部tab按钮--}}
        <div class="card-header">
            <div class="col">
                <ul class="nav nav-tabs nav-overflow header-tabs">
                    <li class="nav-item">
                        <a href="{{route('admin.article.index')}}" class="nav-link">
                            新闻列表
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.article.create')}}" class="nav-link active"
                           style="padding-bottom: 9px;">
                            添加新闻
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <form action="{{route('admin.article.store')}}" method="post">
            @csrf
            {{--表单部分--}}
            <div class="card-body">
                @include('admin.layouts._article')
            </div>
            {{--提交按钮--}}
            <div class="card-footer text-muted">
                <button class="btn btn-primary btn-sm">保存提交</button>
            </div>
        </form>
    </div>
@endsection