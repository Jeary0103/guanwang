@extends('admin.layouts.master')

@section('content')
    <div class="card mt-2">
        {{--头部tab按钮--}}
        <div class="card-header">
            <div class="col">
                <ul class="nav nav-tabs nav-overflow header-tabs">
                    <li class="nav-item">
                        <a href="{{route('admin.about.index')}}" class="nav-link active"
                           style="padding-bottom: 9px;">
                            关于列表
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.about.create')}}" class="nav-link">
                            添加关于
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="card-body">
            <table class="table text-center">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>文章标题</th>
                    <th>所属栏目</th>
                    <th>发表时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($abouts as $about)
                    <tr>
                        <td scope="row">{{$about['id']}}</td>
                        <td style="max-width: 80px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"
                            title="{{$about['title']}}">{{$about['title']}}</td>
                        <td>
                            {{$about->category['title']}}
                        </td>
                        <td>
                            {{$about->created_at->diffForHumans()}}
                        </td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                <a href="{{route('admin.about.edit',$about)}}" class="btn btn-light">编辑关于</a>
                                <button type="button" class="btn btn-white" onclick="del(this)">删除关于</button>
                                <form action="{{route('admin.about.destroy',$about)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer text-muted">
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    {{$abouts->links()}}
                </ul>
            </nav>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function del(obj) {
            if(confirm('确认删除吗?')){
                $(obj).next().submit();
            }
        }
    </script>
@endpush