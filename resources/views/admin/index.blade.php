@extends('admin.layouts.master')
@section('content')
    <div class="card mt-2">
        <div class="card-header">
           信息展示
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-block">
                    <table class="table text-center">
                        <thead>
                        <tr>
                            <th>1</th>
                            <th>1</th>
                            <th>1</th>
                            <th>1</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td scope="row">2</td>
                            <td>2</td>
                            <td>2</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td scope="row">3</td>
                            <td>3</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="card-footer text-muted">
            底部
        </div>
    </div>
@endsection