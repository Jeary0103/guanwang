<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="keywords" content="双猴科技,双猴娱乐,山西双猴科技,山西麻将,洪洞麻将,麻将游戏山西,临汾麻将,临汾麻将"/>
    <meta name="description" content="山西双猴科技是一家专门针对地方性麻将游戏和其他游戏开发的专业游戏开发公司。现在产品
主要有洪洞麻将、口点点、洪洞斗地主等"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport"/>
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>公司概况</title>
    <!-- Bootstrap -->
    <link href="{{asset('org/home')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/bootstrap-me.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/animate.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/style.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/swiper.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--导航-->
@include('home.layouts._header')

<!-- banner -->
<div class="swiper-container about_banner about_banners">
    <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image: url({{asset('org/home')}}/img/about_banner.jpg)"></div>
    </div>
</div>
<!-- main -->
<div class="wrapper about_wrapper tab_wrapper">
    <div class="container">
        <div class="tab_box col-sm-12">
            <div class="tab_details on">
                {{--手机端--}}
                <div class="do_well wow fadeInUp">
                    <div class="container">
                        {{--公司简介--}}
                        @foreach($news_gsjj as $gsjj)

                            <div class="index_titlebox">
                                <h3 class="index_title">{{$cat_gsjj->title}}</h3>
                                <p class="index_intro">{{$gsjj['title']}}</p>
                            </div>

                            <div class="box">
                                {{--图片部分--}}
                                <div class="col-sm-5"><img class="img-responsive"
                                                           src="{{$gsjj['thumb']}}" alt=""></div>
                                {{--公司简介内容--}}
                                <div class="col-sm-7">
                                    <h3></h3>
                                    {!! $gsjj['content'] !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="do_well yctd">
                    <div class="container">
                        <div class="index_titlebox">
                            <h3 class="index_title">{{$cat_gstd->title}}</h3>
                            <p class="index_intro">忠诚、奉献、责任、感恩 </p>
                        </div>

                        {{--公司态度手机端代码--}}
                        <div class="box hidden-lg">
                            @foreach($news_gstd as $gstd)
                                <div class="col-sm-3 col-xs-6 wow fadeInDownBig">
                                    <div class="all_box">
                                        <div class="icon_box">
                                            <i class="icon iconfont">{!! $gstd['icon'] !!}</i>
                                        </div>
                                        <div class="text_box">
                                           {!! $gstd['content'] !!}
                                        </div>
                                    </div>
                                    <div class="title_box">{{$gstd['title']}}</div>
                                </div>
                            @endforeach
                        </div>

                        <!-- 公司态度 PC 端显示的样式 -->
                        <div class="box visible-lg">
                            @foreach($news_gstd as $gstd)
                                <div class="col-sm-3">
                                    <figure class="imghvr-flip-diag-2"><img
                                                src="{{$gstd['thumb']}}" alt="example-image">
                                        <figcaption>
                                            <div><i class="icon iconfont">{!! $gstd['icon'] !!}</i></div>
                                            <h3>{{$gstd['title']}}</h3>
                                            {!! $gstd['content'] !!}
                                        </figcaption>
                                    </figure>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab_details">
                2
            </div>
        </div>
    </div>
</div>

@include('home.layouts._footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('org/home')}}/js/jquery-1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('org/home')}}/js/bootstrap.min.js"></script>
<script src="{{asset('org/home')}}/js/swiper.min.js"></script>
<script src="{{asset('org/home')}}/js/jquery.singlePageNav.min.js"></script>
<script src="{{asset('org/home')}}/js/wow.min.js"></script>
<script src="{{asset('org/home')}}/js/common.js"></script>

</body>
</html>