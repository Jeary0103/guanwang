<!-- 最顶部二维码一行 -->
<div class="top visible-lg">
    <div class="container">
        <div class="pull-left">欢迎来到山西双猴科技有限公司！</div>
        <div class="pull-right">
            <ul class="list-inline">
                <li class="app"><i class="icon iconfont">&#xece6;</i> 手机官网</li>
                <li class="code">
                    <i class="icon iconfont">&#xece4;</i> 关注微信公众号 <i class="icon iconfont">&#xea33;</i>
                    <div class="code_img"><img src="{{asset('org/home')}}/img/erweima.png" alt=""></div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--导航开始-->
<nav class="navbar navbar-default">
    <div class="container">
        <!-- 小屏幕导航按钮和LOGO -->
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.html" class="navbar-brand"><img src="{{asset('org/home')}}/img/logo2.png" alt=""></a>
        </div>
        <!-- 小屏幕导航按钮和LOGO -->

        <!-- 大屏幕导航 -->
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a class="{{active_class(if_route('home.index'))}}" href="{{route('home.index')}}">首页</a></li>
                <li><a class="{{active_class(if_route('home.about'))}}" href="{{route('home.about')}}">公司概况</a></li>
                <li><a class="{{active_class(if_route('home.service'))}}" href="{{route('home.service')}}">加盟我们</a></li>
                <li><a class="{{active_class(if_route('home.case_list'))}}" href="{{route('home.case_list')}}">案例展示</a></li>
                {{--<li><a class="{{active_class(if_route('home.games'))}}" href="{{route('home.games')}}">游戏专题</a></li>--}}
                <li><a class="{{active_class(if_route('home.news'))}}" href="{{route('home.news')}}">新闻动态</a></li>
                <li><a class="{{active_class(if_route('home.links'))}}" href="{{route('home.links')}}">联系我们</a></li>
                <li><a class="" href="http://sh.szdjjy.cn">游戏商城</a></li>
            </ul>
        </div>
        <!-- 大屏幕导航 -->
    </div>
</nav>
<!--导航结束-->