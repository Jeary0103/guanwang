<!-- 腰线2 -->
{{--<div class="waist_line2"></div>--}}
<!-- 底部 -->
<div class="footer visible-lg">
    <div class="container">
        <ul class="list-inline">
            <li><a href="{{route('home.index')}}">网站首页</a><span>|</span></li>
            <li><a href="{{route('home.about')}}">关于我们</a><span>|</span></li>
            <li><a href="{{route('home.case_list')}}">成功案例</a><span>|</span></li>
            <li><a href="{{route('home.service')}}">加盟我们</a><span>|</span></li>
            <li><a href="{{route('home.news')}}">新闻中心</a><span>|</span></li>
            <li><a href="javascript:;">预留位置1</a><span>|</span></li>
            <li><a href="javascript:;">联系我们</a><span>|</span></li>
            <li><a href="javascript:;">友情链接</a><span>|</span></li>
            <li><a href="javascript:;">游戏商城</a></li>
        </ul>
        <div class="row">
            <div class="col-sm-5">
                山西双猴科技有限公司</br>
                电话：0351-7337891</br>
                地址：太原市万柏林区千峰南路长风观园二期</br>
                邮编：030000
            </div>
            <div class="col-sm-4">
                E-mail：237618121@qq.com</br>
                网址：http://shuanghou.0349edu.cn</br>
                版权：山西双猴科技有限公司</br>
                备案：晋ICP备180485999
            </div>
            <div class="col-sm-3">
                <img src="{{asset('org/home')}}/img/erweima.png" alt="">
                <p>扫一扫，关注我们</p>
            </div>
        </div>
    </div>
</div>
<div class="copyright visible-lg">
    <div class="container">Copyright&copy;2018 山西双猴科技 All rights reserved. </div>
</div>
<div class="copyright visible-xs">
    <div class="container">
        <p>版权所有&copy;山西双猴科技</p>
        <p>晋ICP备18048599号</p>
    </div>
</div>