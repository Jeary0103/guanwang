
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport" />
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>加盟我们</title>
    <!-- Bootstrap -->
    <link href="{{asset('org/home')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/bootstrap-me.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/animate.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/style.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/swiper.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

@include('home.layouts._header')


<!-- banner -->
<div class="swiper-container about_banner">
    <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image: url({{asset('org/home')}}/img/service_02.png)"></div>
    </div>
</div>
<!-- main -->
<div class="service_wrapper">
    <div class="container">
        <!-- 我们可以做的更好 -->
        <div class="do_well">
            <div class="container">
                <div class="index_titlebox">
                    <h3 class="index_title">我们的特色</h3>
                    <p class="index_intro">我们为您提供更具有优势的游戏网络服务平台</p>
                </div>
                <section class="business active visible-lg">
                    <div class="box">
                        <ul class="items list-inline">
                            <li class="pc">
                                <u class="cl"></u>
                                <u class="cr"></u>
                                <i></i><strong>高端游戏定制开发</strong>
                                <p>属于你的高端定制游戏开发<br />彰显个人魅力</p>
                            </li>
                            <li class="mobi">
                                <u class="cl"></u>
                                <u class="cr"></u>
                                <i></i><strong>大家都说好玩</strong>
                                <p>好友约局对战<br />轻松就能进行互动</p>
                            </li>
                            <li class="sys">
                                <u class="cl"></u>
                                <u class="cr"></u>
                                <i></i><strong>我-就是这么任性</strong>
                                <p>一部手机，一个微信<br />随时随地都可以畅玩</p>
                            </li>
                            <li class="app">
                                <u class="cl"></u>
                                <u class="cr"></u>
                                <i></i><strong>安全有保障</strong>
                                <p>安全可靠的游戏环境<br />&nbsp;&nbsp;&nbsp;&nbsp;没有任何作弊的问题。</p>

                            </li>
                        </ul>
                    </div>
                </section>
                <section class="service_dowell hidden-lg">
                    <div class="col-sm-3 website wow fadeInLeft ">
                        <h5></h5>
                        <h4>高端游戏定制开发</h4>
                        <p>属于你的高端定制游戏开发<br />彰显个人魅力</p>
                    </div>
                    <div class="col-sm-3 app wow fadeInRight ">
                        <h5></h5>
                        <h4>大家都说好玩</h4>
                        <p>好友约局对战<br />轻松就能进行互动</p>
                    </div>
                    <div class="col-sm-3 h5 wow fadeInLeft ">
                        <h5></h5>
                        <h4>我-就是这么任性</h4>
                        <p>一部手机，一个微信<br />随时随地都可以畅玩</p>
                    </div>
                    <div class="col-sm-3 wechat wow fadeInRight ">
                        <h5></h5>
                        <h4>安全有保障</h4>
                        <p>安全可靠的游戏环境<br />没有任何作弊的问题。</p>
                    </div>
                    <div class="clearfix"></div>
                </section>
            </div>
        </div>
        <!-- 创意与创新的桥梁 -->
        {{--<div class="do_well">--}}
            {{--<div class="container">--}}
                {{--<div class="index_titlebox">--}}
                    {{--<h3 class="index_title">创意与创新的桥梁</h3>--}}
                    {{--<p class="index_intro">根据一定要求，完成视觉传达的目的</p>--}}
                {{--</div>--}}
                {{--<div class="box">--}}
                    {{--<div class="col-sm-4 col-xs-6  wow rotateIn ">--}}
                        {{--<a href="#">--}}
                            {{--<div class="img_box"><img class="img-responsive" src="{{asset('org/home')}}/img/service_05.png" alt=""></div>--}}
                            {{--<div class="text_box">--}}
                                {{--<h4>人性化</h4>--}}
                                {{--<p>通过色彩处理、主图创意、布局创新、版块合理规划等方式,使网站平台更利于客户浏览.--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-4 col-xs-6 col-xs-6  wow rotateIn ">--}}
                        {{--<a href="#">--}}
                            {{--<div class="img_box"><img class="img-responsive" src="{{asset('org/home')}}/img/service_07.png" alt=""></div>--}}
                            {{--<div class="text_box">--}}
                                {{--<h4>交互性</h4>--}}
                                {{--<p>建立异步沟通系统（帮助中心、留言板、问题解答…）方便客户与网站之间...--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="cb"></div>--}}
                    {{--<div class="col-sm-4 col-xs-6 col-xs-6  wow rotateIn ">--}}
                        {{--<a href="#">--}}
                            {{--<div class="img_box"><img class="img-responsive" src="{{asset('org/home')}}/img/service_09.png" alt=""></div>--}}
                            {{--<div class="text_box">--}}
                                {{--<h4>逻辑性</h4>--}}
                                {{--<p>整体网站策划流程符合客户心理，且在充分分析客户行业特征的基础上，建立网站内部功能...--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix hidden-xs"></div>--}}
                    {{--<div class="col-sm-4 col-xs-6 col-xs-6  wow rotateIn ">--}}
                        {{--<a href="#">--}}
                            {{--<div class="img_box"><img class="img-responsive" src="{{asset('org/home')}}/img/service_14.png" alt=""></div>--}}
                            {{--<div class="text_box">--}}
                                {{--<h4>营销性</h4>--}}
                                {{--<p>整合用户体验式浏览及营销推广功能，从客户访问到咨询产生形成高转化率。--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="cb"></div>--}}
                    {{--<div class="col-sm-4 col-xs-6 col-xs-6  wow rotateIn ">--}}
                        {{--<a href="#">--}}
                            {{--<div class="img_box"><img class="img-responsive" src="{{asset('org/home')}}/img/service_15.png" alt=""></div>--}}
                            {{--<div class="text_box">--}}
                                {{--<h4>控制性</h4>--}}
                                {{--<p>网站各栏目及分类清晰，便于统计客户访问页面深度、停留时间、产品或服务关注度、点击...--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-4 col-xs-6 col-xs-6  wow rotateIn ">--}}
                        {{--<a href="#">--}}
                            {{--<div class="img_box"><img class="img-responsive" src="{{asset('org/home')}}/img/service_17.png" alt=""></div>--}}
                            {{--<div class="text_box">--}}
                                {{--<h4>界面友好性</h4>--}}
                                {{--<p>心理学表明，适合的界面对于客户点击欲望的提升和印象指数的提升效果明显，网站界面是...--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix hidden-xs"></div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- 我们的服务品质 -->
        <div class="do_well">
            <div class="container">
                <div class="index_titlebox">
                    <h3 class="index_title">我们的模式</h3>
                    <p class="index_intro">只要您...有梦想、热爱棋牌、够多的麻友、就能轻松过万！</p>
                </div>
                <div class="box hidden-xs">
                    <div class="col-sm-4">
                        <div class="he_3DFlipY">
                            <div class="he_3DFlipY_inner">
                                <div class="he_3DFlipY_img">
                                    <span><i class="icon iconfont">&#xec9c;</i></span>
                                    <span><i class="icon iconfont">&#xeca9;</i></span>
                                    <span><i class="icon iconfont">&#xecce;</i></span>
                                    <p class="p2">
                                        <span style="color: #6C6C6C; font-size: 1.2em;">一级代理</span><br/>
                                        注册量达3000人<br/>
                                        1500人平均在线<br/>
                                        70%分成
                                    </p>
                                </div>
                                <div class="he_3DFlipY_caption">
                                    <span><i class="icon iconfont">&#xec9c;</i></span>
                                    <span><i class="icon iconfont">&#xeca9;</i></span>
                                    <span><i class="icon iconfont">&#xecce;</i></span>
                                    <p class="p2">
                                        一级代理<br/>
                                        注册量达3000人<br/>
                                        1500人平均在线<br/>
                                        70%分成
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="he_3DFlipY">
                            <div class="he_3DFlipY_inner">
                                <div class="he_3DFlipY_img">
                                    <span><i class="icon iconfont">&#xecc6;</i></span>
                                    <span><i class="icon iconfont">&#xecaa;</i></span>
                                    <span><i class="icon iconfont">&#xecb9;</i></span>
                                    <p class="p2">
                                        <span style="color: #6C6C6C; font-size: 1.2em;">二级代理</span><br/>
                                        注册量达1000人<br/>
                                        500人平均在线<br/>
                                        60%分成
                                    </p>
                                </div>
                                <div class="he_3DFlipY_caption">
                                    <span><i class="icon iconfont">&#xecc6;</i></span>
                                    <span><i class="icon iconfont">&#xecaa;</i></span>
                                    <span><i class="icon iconfont">&#xecb9;</i></span>
                                    <p class="p2">
                                        二级代理<br/>
                                        注册量达1000人<br/>
                                        500人平均在线<br/>
                                        60%分成
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="he_3DFlipY">
                            <div class="he_3DFlipY_inner">
                                <div class="he_3DFlipY_img">
                                    <span><i class="icon iconfont">&#xeca2;</i></span>
                                    <span><i class="icon iconfont">&#xecb6;</i></span>
                                    <span><i class="icon iconfont">&#xecb4;</i></span>
                                    <p class="p2">
                                        <span style="color: #6C6C6C; font-size: 1.2em;">三级代理</span><br/>
                                        注册量达500人<br/>
                                        250人平均在线<br/>
                                        50%分成
                                    </p>
                                </div>
                                <div class="he_3DFlipY_caption">
                                    <span><i class="icon iconfont">&#xeca2;</i></span>
                                    <span><i class="icon iconfont">&#xecb6;</i></span>
                                    <span><i class="icon iconfont">&#xecb4;</i></span>
                                    <p class="p2">
                                        三级代理<br/>
                                        注册量达500人<br/>
                                        250人平均在线<br/>
                                        50%分成
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="quality visible-xs">
                    <div class="media">
                        <a href="#">
                            <div class="media-left  wow fadeInLeft ">
                                <img class="media-object" data-src="holder.js/98x98" src="{{asset('org/home')}}/img/dnzd.png"  style="width: 98px; height: 98px;">
                            </div>
                            <div class="media-body wow fadeInRight ">
                                <p>
                                    <span> 一级代理</span><br>
                                    <span>注册量达3000人</span><br>
                                    <span>1500人平均在线</span><br>
                                    <span> 70%分成</span>
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="media">
                        <a href="#">
                            <div class="media-left wow fadeInLeft ">
                                <img class="media-object" data-src="holder.js/98x98" src="{{asset('org/home')}}/img/llq.png"  style="width: 98px; height: 98px;">
                            </div>
                            <div class="media-body wow fadeInRight ">
                                <p>
                                    <span>二级代理</span><br>
                                    <span>注册量达1000人</span><br>
                                    <span>500人平均在线</span><br>
                                    <span>60%分成</span><br>
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="media">
                        <a href="#">
                            <div class="media-left  wow fadeInLeft ">
                                <img class="media-object" data-src="holder.js/98x98" src="{{asset('org/home')}}/img/jg.png"  style="width: 98px; height: 98px;">
                            </div>
                            <div class="media-body  wow fadeInRight ">
                                <p>
                                    <span>三级代理</span><br>
                                    <span> 注册量达500人</span><br>
                                    <span>250人平均在线</span><br>
                                    <span>50%分成</span>
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('home.layouts._footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('org/home')}}/js/jquery-1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('org/home')}}/js/bootstrap.min.js"></script>
<script src="{{asset('org/home')}}/js/swiper.min.js"></script>
<script src="{{asset('org/home')}}/js/jquery.singlePageNav.min.js"></script>
<script src="{{asset('org/home')}}/js/wow.min.js"></script>
<script src="{{asset('org/home')}}/js/common.js"></script>
</body>
</html>