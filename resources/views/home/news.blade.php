<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport"/>
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>新闻动态</title>
    <!-- Bootstrap -->
    <link href="{{asset('org/home')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/bootstrap-me.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/animate.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/style.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/swiper.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

{{--头部导航开始--}}
@include('home.layouts._header')
{{--头部导航结束--}}

<!-- banner -->
<div class="swiper-container about_banner">
    <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image: url({{asset('org/home')}}/img/news_banner.png)"></div>
    </div>
</div>
<!-- main -->
<div class="wrapper tab_wrapper news_wrapper">
    <div class="container">
        {{--<div align="center" class="tab_nav">--}}
        {{--<ul class="list-inline tab">--}}
        {{--<li class="cur"><a href="#">企业新闻</a></li>--}}
        {{--<li><a href="#">企业公告</a> </li>--}}
        {{--<li class="last"><a href="#">行业新闻</a> </li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        <div class="tab_box" style="margin-top: 50px;">
            {{--手机端--}}
            <div class="tab_details on">
                <ul class="list-unstyled">
                    @foreach($news_xwdt as $xwdt)
                        <li class=" wow fadeInUp">
                            <a href="{{route('home.news_detail',$xwdt['id'])}}">
                                <div class="imgPic">
                                    <img src="{{$xwdt['thumb']}}" height="145" width="225">
                                </div>
                                <div class="listInfo">
                                    <div class="nTitile">{{$xwdt['title']}}</div>
                                    <div class="n_bottom" style="margin-top: 20px;">
                                        <div class="pull-left">
                                            <button class="btn" href="#">查看详情&gt;&gt;</button>
                                        </div>
                                        <div class="pull-right">
                                            <span ><i style="margin-right:4px;" class="icon iconfont">&#xe599;</i>{{$xwdt['created_at']}}</span>
                                            <span><i class="icon iconfont">&#xec84;</i> {{$xwdt['click']}}</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

@include('home.layouts._footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('org/home')}}/js/jquery-1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('org/home')}}/js/bootstrap.min.js"></script>
<script src="{{asset('org/home')}}/js/swiper.min.js"></script>
<script src="{{asset('org/home')}}/js/jquery.singlePageNav.min.js"></script>
<script src="{{asset('org/home')}}/js/wow.min.js"></script>
<script src="{{asset('org/home')}}/js/common.js"></script>
</body>
</html>