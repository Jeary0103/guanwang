<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport" />
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>游戏专题</title>
    <!-- Bootstrap -->
    <link href="{{asset('org/home')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/bootstrap-me.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/animate.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/style.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/swiper.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

@include('home.layouts._header')

<!-- banner -->
<div class="swiper-container about_banner">
    <div class="swiper-wrapper">
        <div class="swiper-slide"  style="background-image: url({{asset('org/home')}}/img/modal_banner.jpg)"></div>
    </div>
</div>
<!-- main -->
<div class="wrapper case_wrapper tab_wrapper">
    <div class="container">
        {{--<div align="center" class="tab_nav">--}}
            {{--<ul class="list-inline tab">--}}
                {{--<li class="cur"><a href="#">全部</a></li>--}}
                {{--<li><a href="#">网站建设</a></li>--}}
                {{--<li><a href="#">移动APP</a></li>--}}
                {{--<li class="last"><a href="#">品牌&平面</a> </li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        <div class="tab_box modal_show col-sm-12">
            <div class="tab_details on">
                <div class="row">
                    <div class="col-sm-4 col-xs-6 wow fadeInUp ">
                        <a href="#">
                            <div class="nr">
                                <div class="img_boxs"><img class="img-responsive" src="{{asset('org/home')}}/img/modal_img.png" alt="" /></div>
                                <div class="text_boxs">
                                    <h4>中国投资论坛网</h4>
                                    <p class="p1">网站地址：www.yuchentimes.com</p>
                                    <p class="p2">
                                        详情：农业平台网站是目前农机、农资等供应求购信息发布比较全面的网站，方便了...信息发布较全的网站...
                                    </p>
                                    <p>点击次数：196</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-6 wow fadeInUp ">
                        <a href="#">
                            <div class="nr">
                                <div class="img_boxs"><img class="img-responsive" src="{{asset('org/home')}}/img/modal_img.png" alt="" /></div>
                                <div class="text_boxs">
                                    <h4>中国投资论坛网</h4>
                                    <p class="p1">网站地址：www.yuchentimes.com</p>
                                    <p class="p2">
                                        详情：农业平台网站是目前农机、农资等供应求购信息发布比较全面的网站，方便了...信息发布较全的网站...
                                    </p>
                                    <p>点击次数：196</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-6 wow fadeInUp ">
                        <a href="#">
                            <div class="nr">
                                <div class="img_boxs"><img class="img-responsive" src="{{asset('org/home')}}/img/modal_img.png" alt="" /></div>
                                <div class="text_boxs">
                                    <h4>中国投资论坛网</h4>
                                    <p class="p1">网站地址：www.yuchentimes.com</p>
                                    <p class="p2">
                                        详情：农业平台网站是目前农机、农资等供应求购信息发布比较全面的网站，方便了...信息发布较全的网站...
                                    </p>
                                    <p>点击次数：196</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="tab_details">
                2
            </div>
            <div class="tab_details">
                3
            </div>
            <div class="tab_details">
                4
            </div>
            <div class="tab_details">
                5
            </div>
            <div class="tab_details">
                6
            </div>
            <div class="tab_details">
                7
            </div>
        </div>
    </div>
</div>

@include('home.layouts._footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('org/home')}}/js/jquery-1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('org/home')}}/js/bootstrap.min.js"></script>
<script src="{{asset('org/home')}}/js/swiper.min.js"></script>
<script src="{{asset('org/home')}}/js/jquery.singlePageNav.min.js"></script>
<script src="{{asset('org/home')}}/js/wow.min.js"></script>
<script src="{{asset('org/home')}}/js/common.js"></script>
</body>
</html>