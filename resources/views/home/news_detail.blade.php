<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport" />
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>新闻详情</title>
    <!-- Bootstrap -->
    <link href="{{asset('org/home')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/bootstrap-me.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/animate.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/style.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/swiper.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

@include('home.layouts._header')
<!-- banner -->
<div class="swiper-container about_banner">
    <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image: url({{asset('org/home')}}/img/news_banner.png)"></div>
    </div>
</div>
<!-- main -->
<div class="news_details">
    <div class="container">
        <div class="title">
            <h4 class="tc">{{$article['title']}}</h4>
            <div class="tc"><span><i class="icon iconfont">&#xecf4;</i> {{$article->created_at}}</span> <span><i class="icon iconfont">&#xec84;</i> {{$article['click']}}</span></div>
        </div>
        <div class="text_box">
           {!! $article->content !!}
            <img class="img-responsive" src="{{$article['thumb']}}" alt="">
        </div>
        {{--<div class="tc article_btn">--}}
            {{--<div class="pull-left"><a href="#">上一篇：国际消费者权益日即3.15，大众“途锐”被举报发动机进水严重质量问题</a></div>--}}
            {{--<div class="pull-right"><a href="#">上一篇：国际消费者权益日即3.15，大众“途锐”被举报发动机进水严重质量问题</a></div>--}}
        {{--</div>--}}
    </div>
</div>

@include('home.layouts._footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('org/home')}}/js/jquery-1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('org/home')}}/js/bootstrap.min.js"></script>
<script src="{{asset('org/home')}}/js/swiper.min.js"></script>
<script src="{{asset('org/home')}}/js/jquery.singlePageNav.min.js"></script>
<script src="{{asset('org/home')}}/js/wow.min.js"></script>
<script src="{{asset('org/home')}}/js/common.js"></script>
</body>
</html>