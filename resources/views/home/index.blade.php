<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="keywords" content="双猴科技,双猴娱乐,山西双猴科技,山西麻将,洪洞麻将,麻将游戏山西,临汾麻将,临汾麻将"/>
    <meta name="description" content="山西双猴科技是一家专门针对地方性麻将游戏和其他游戏开发的专业游戏开发公司。现在产品
主要有洪洞麻将、口点点、洪洞斗地主等"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport"/>
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>山西双猴官网首页</title>
    <!-- Bootstrap -->
    <link href="{{asset('org/home')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/bootstrap-me.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/animate.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/style.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/swiper.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="lala">

@include('home.layouts._header')

<!-- Swiper banner 手机端响应时的图片显示代码 -->
<div class="swiper-container wow fadeIn hidden-lg">
    <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image: url({{asset('org/home')}}/img/banner1_home.jpg)"></div>
        <div class="swiper-slide" style="background-image: url({{asset('org/home')}}/img/banner02.jpg)"></div>
        <div class="swiper-slide" style="background-image: url({{asset('org/home')}}/img/banner03.jpg)"></div>
    </div>
    <div class="swiper-pagination swiper-pagination-white"></div>
</div>

<!-- banner 大分辩显示  PC端显示代码-->
<div class="wrap_banner visible-lg">
    <div class="bpic" style="opacity:1;">
        <div><a href="#"><img src="{{asset('org/home')}}/img/banner1_home.jpg"></a></div>
    </div>

    <div class="b1 scroll">
        <ul class="list-unstyled" style="left:0px;">
            <li class="active">
                <img src="{{asset('org/home')}}/img/banner01.png">
                <p>专业订制</p>
                <div><a href="#"><img src="{{asset('org/home')}}/img/banner1_home.jpg"></a></div>
            </li>
            <li>
                <img src="{{asset('org/home')}}/img/banner_02.jpg">
                <p>本土玩法</p>
                <div><a href="#"><img src="{{asset('org/home')}}/img/banner02.jpg"></a></div>
            </li>
            <li>
                <img src="{{asset('org/home')}}/img/banner_03.jpg">
                <p>游戏预览</p>
                <div><a href="#"><img src="{{asset('org/home')}}/img/banner03.jpg"></a></div>
            </li>
        </ul>
    </div>
</div>
<!-- 我们可以做的更好 -->
<div class="do_well">
    <div class="container">
        <div class="index_titlebox">
            <h3 class="index_title">我们可以做得更好</h3>
            <p class="index_intro">我们为您提供更具优势的游戏服务平台</p>
        </div>
        <div class="box index_box">
            <ul class="list-inline">
                <li class="col-sm-3 col-xs-3 wow bounceInRight">
                    <div>
                        <img class="img-responsive img1" src="{{asset('org/home')}}/img/index (2).png" alt="">
                        <img class="img-responsive img2" src="{{asset('org/home')}}/img/index (2).png" alt="">
                    </div>
                    <p class="p4">最具专业的游戏开发</p>
                </li>
                <li class="col-sm-3 col-xs-3 wow bounceInLeft">
                    <div>
                        <img class="img-responsive img1" src="{{asset('org/home')}}/img/index (3).png" alt="">
                        <img class="img-responsive img2" src="{{asset('org/home')}}/img/index (3).png" alt="">
                    </div>
                    <p class="p1">优秀的兼容性</p>
                </li>
                <li class="col-sm-3 col-xs-3 wow bounceInLeft">
                    <div>
                        <img class="img-responsive img1" src="{{asset('org/home')}}/img/index (5).png" alt="">
                        <img class="img-responsive img2" src="{{asset('org/home')}}/img/index (5).png" alt="">
                    </div>
                    <p class="p2">属于你我的专属游戏</p>
                </li>
                <li class="col-sm-3 col-xs-3 wow bounceInRight">
                    <div>
                        <img class="img-responsive img1" src="{{asset('org/home')}}/img/index (4).png" alt="">
                        <img class="img-responsive img2" src="{{asset('org/home')}}/img/index (4).png" alt="">
                    </div>
                    <p class="p3">为您量身定制</p>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- 我们的成功案例 -->
<div class="do_well">
    <div class="container">
        <div class="index_titlebox">
            <h3 class="index_title">我们的成功案例</h3>
            <p class="index_intro">专业的团队，为您提供专业的服务</p>
        </div>
        <div class="box">
            <div class="items-container">
                @foreach($news_alzs as $alzs)
                    <a href="{{route('home.case_detail',$alzs['id'])}}">
                    <div class="item col-sm-4 col-xs-6 wow fadeInDown">
                        <div class="image01">
                            <img src="{{$alzs['thumb']}}" alt="">
                            <div class="title">{{$alzs['title']}}</div>
                            <div class="ovrly"></div>
                            <div class="buttons">
                                <p>{{$alzs['title']}}</p>
                                <div class="tc"><i class="icon iconfont">&#xeb10;</i></div>
                            </div>
                        </div>
                    </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- 我们是设计师、工程师、梦想缔造者 也是您的互联网服务保障 -->
<div class="waist_line">
    <h2 class="tc wow fadeInDown">我们是设计师、工程师、梦想缔造者 </br>更是为您保驾护航的服务者</h2>
    <div align="center" class="tc index_button wow fadeInUp">
        <a class="btn" href="{{route('home.links')}}">了解更多</a>
        <a class="btn" href="{{route('home.links')}}">咨询我们</a>
    </div>
</div>

@include('home.layouts._footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('org/home')}}/js/jquery-1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('org/home')}}/js/bootstrap.min.js"></script>
<script src="{{asset('org/home')}}/js/swiper.min.js"></script>
<script src="{{asset('org/home')}}/js/jquery.singlePageNav.min.js"></script>
<script src="{{asset('org/home')}}/js/wow.min.js"></script>
<script src="{{asset('org/home')}}/js/common.js"></script>
<script src="{{asset('org/home')}}/js/jquery.tools.min.js" type="text/javascript"></script>
<script src="{{asset('org/home')}}/js/script.js" type="text/javascript"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 30,
        effect: 'fade',
        centeredSlides: true,
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

</script>
</body>
</html>