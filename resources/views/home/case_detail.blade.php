<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="keywords" content="双猴科技,双猴娱乐,山西双猴科技,山西麻将,洪洞麻将,麻将游戏山西,临汾麻将,临汾麻将"/>
    <meta name="description" content="山西双猴科技是一家专门针对地方性麻将游戏和其他游戏开发的专业游戏开发公司。现在产品
主要有洪洞麻将、口点点、洪洞斗地主等"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport" />
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>案例详情</title>
    <!-- Bootstrap -->
    <link href="{{asset('org/home')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/bootstrap-me.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/animate.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/style.css" rel="stylesheet">
    <link href="{{asset('org/home')}}/css/swiper.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<script>
    onload = function() {
        document.documentElement.style.fontSize = innerWidth / 16 + 'px';
        onresize = function() {
            document.documentElement.style.fontSize = innerWidth / 16 + 'px';
        }
    }
</script>
<body>

@include('home.layouts._header')

<!-- banner -->
<div class="swiper-container about_banner">
    <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image: url({{asset('org/home')}}/img/case_banner.png)"></div>
    </div>
</div>
<!-- main -->
<div class="wrapper case_details">
    <div class="container">

        <div class="text_box">
            <div class="title">
                <h3 class="tc"></h3>
                <a href="javascript:;" class="btn">访问网站</a>
            </div>
            {{--PC端显示代码--}}
            <div class="lbt hidden-xs">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img  alt="First slide [900x500]" src="{{$article['thumb']}}" data-holder-rendered="true">
                        </div>
                        <div class="item">
                            <img  alt="First slide [900x500]" src="{{asset('org/home')}}/img/case_detail01.jpg" data-holder-rendered="true">
                        </div>
                        <div class="item">
                            <img  alt="First slide [900x500]" src="{{asset('org/home')}}/img/case_detail02.jpg" data-holder-rendered="true">
                        </div>
                    </div>
                </div>
            </div>
            <!-- 移动端下显示样式 -->
            <div class="case_wap visible-xs">
                <div class="col-sm-12">
                    <img class="img-responsive" src="{{$article['thumb']}}" alt="">
                </div>
            </div>
        </div>
        <div class="case_tip tc">
            <h3>{{$article['title']}}</h3>
            <a href="{{route('home.links')}}" class="btn x_gradient">我要私人定制</a>
        </div>
    </div>
</div>

@include('home.layouts._footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('org/home')}}/js/jquery-1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('org/home')}}/js/bootstrap.min.js"></script>
<script src="{{asset('org/home')}}/js/swiper.min.js"></script>
<script src="{{asset('org/home')}}/js/jquery.singlePageNav.min.js"></script>
<script src="{{asset('org/home')}}/js/wow.min.js"></script>
<script src="{{asset('org/home')}}/js/common.js"></script>

</body>
</html>