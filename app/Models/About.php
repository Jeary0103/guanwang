<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
        'title',
        'thumb',
        'category_id',
        'content',
        'fields'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
