<?php

namespace App\Models;

use Houdunwang\Arr\Arr;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // 允许填充的字段值
    protected $fillable = [
        'title',
        'parent_id',
        'description',
        'is_home',
        'home_template',
        'list_template',
        'content_template',
        'content_model_id',
    ];

    /**
     * 获取栏目的树状结构
     * 注意：当报错一个静态方法找不到的时候，说明是没有这种静态写法，所以我们要先实例化一个对象
     * 出来，然后用对象调用方法的形式来声明调用。
     */
    public function getTreeCategory()
    {
        // 整体思路就是拿 循环的数据跟当前编辑的数据进行比较

        // 获取所有分类数据
        $categoryAll = self::get()->toArray();
        // 实例化一个Arr类
        $arr = new Arr();
        // 树状化栏目分类数据
        $categories = $arr->tree($categoryAll, 'title', 'id', 'parent_id');
        // 对得到的树状结构分类数据进行循环展示，
        // 并且加上临时属性 是否是 子类 或者 是父类 的属性和值。
        foreach ($categories as $k => $v) {
            // 判断当前分类是否是父类
            $v['is_parent'] = $v['id'] == $this['parent_id'];
            // 判断当前分类是否为子类
            $v['is_child'] = $arr->isChild($categories, $v['id'], $this['id'], $fieldPri = 'id', $fieldPid = 'parent_id');
            $categories[$k] = $v;
        }
        return $categories;
    }

    /**
     *  获取所有栏目的父级栏目
     *  站在子类的角度看，找父类，那么应该是一对多的问题，所以使用belongsTo魔性关联来
     *  获取父级栏目的数据就可以了
     */
    public function parent_category()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    /**
     * 获取某一个分类下面的数据的方法
     */
    public function getCategoryNews($category_id){
        $news = Article::where('category_id',$category_id)->get();
        return $news;
    }




}
