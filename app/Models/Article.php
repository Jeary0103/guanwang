<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $fillable = [
        'user_id',
        'title',
        'category_id',
        'source',
        'author',
        'description',
        'thumb',
        'content',
        'redirect_url',
        'click',
        'fields',
        'rule_id',
        'is_top',
        'icon'
    ];

    //文章关联分类的方法
    public function category(){
        // 如果关联字段是以 方法名_id 的命名格式，那么外键和内键都可以省略的
        return $this->belongsTo(Category::class);
    }



}
