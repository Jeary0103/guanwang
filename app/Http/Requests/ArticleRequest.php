<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'redirect_url' => 'nullable|url',
            'is_top' => 'required',
            'category_id'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => '文章标题不能为空',
            'content.required' => '文章内容不能为空',
            'redirect_url.url' => '跳转链接网址格式错误',
            'is_top.required' => '请选择是否置顶',
            'cagegory_id.required' => '请选择所属分类'

        ];
    }
}
