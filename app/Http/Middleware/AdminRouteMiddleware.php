<?php

namespace App\Http\Middleware;

use Closure;

class AdminRouteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->check()){
            return redirect(route('user.login'))->with('error','请登陆以后再来');
        }
        return $next($request);
    }
}
