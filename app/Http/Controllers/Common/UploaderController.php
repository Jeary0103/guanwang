<?php

namespace App\Http\Controllers\Common;

use App\Models\Upload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UploaderController extends Controller
{

//    上传图片的方法
    public function make(Request $request, Upload $upload)
    {
        $dir = 'upload/'.date('ym');
        $fillName = auth()->id().'-'.str_random(10).time().'.'.$request->file->getClientOriginalExtension();
        // 返回一个新文件对象
        $file = $request->file->move($dir, $fillName);

        $upload['user_id'] = auth()->id();
        $upload['path'] = url($file);
        $upload['url'] = url($file);
        $upload['name'] = $request->file->getClientOriginalName();
        $upload->save();

        // 返回值是一个数组形式的值
        return ['file' => url($file), 'code' => 0];
    }


//    展示上传图片的方法
    public function lists()
    {
        $uploads = Upload::paginate(10);
        return [
            'data' => $uploads->toArray()['data'],
            'page' => $uploads->links().'',
            'code' => 0,
        ];
    }
}
