<?php

namespace App\Http\Controllers\Home;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    //加载首页方法
    public function index(){
        $cat_alzs = Category::where('title','案例展示')->first();
        $news_alzs = $cat_alzs->getCategoryNews($cat_alzs['id']);

        $cat_xwdt = Category::where('title','新闻动态')->first();
        $news_xwdt = $cat_xwdt->getCategoryNews($cat_xwdt['id']);

        return view('home.index',compact('news_alzs','news_xwdt'));

    }

    // 加载关于我们控制器
    public function about(Category $category){
        $cat_gsjj = Category::where('title','公司简介')->first();
        $news_gsjj = $cat_gsjj->getCategoryNews($cat_gsjj['id']);

        $cat_gstd = Category::where('title','公司态度')->first();
        $news_gstd =  $cat_gstd->getCategoryNews($cat_gstd['id']);

        return view('home.about',compact('news_gsjj','cat_gsjj','cat_gstd','news_gstd'));
    }

    // 加载新闻页面的方法
    public function news(){
        $cat_xwdt = Category::where('title','新闻动态')->first();
        $news_xwdt = $cat_xwdt->getCategoryNews($cat_xwdt['id']);
        return view('home.news',compact('news_xwdt'));
    }

    public function news_detail(Article $article){
        return view('home.news_detail',compact('article'));
    }



    // 加载案例展示页面的方法
    public function case_list(){
        $cat_alzs = Category::where('title','案例展示')->first();
        $news_alzs = $cat_alzs->getCategoryNews($cat_alzs['id']);

        return view('home.case_list',compact('news_alzs'));
    }

    // 加载案例详情的方法
    public function case_detail(Article $article){

        return view('home.case_detail',compact('article'));
    }

    // 加载服务项目的方法
    public function service(){
        return view('home.service');
    }

    // 加载游戏专题页面的方法
    public function games(){
        return view('home.games');
    }

    // 加载联系我们页面
    public function links(){
        return view('home.links');
    }
}
