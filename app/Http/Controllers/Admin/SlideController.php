<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SlideRequest;
use App\Models\Slide;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $slides = Slide::get();
        return view('admin.slide_index',compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.slide_create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SlideRequest $request)
    {
       Slide::create($request->all());
       return redirect(route('admin.slide.create'))->with('success','添加成功！');
    }

    /**
     * Display the specified resource.
     *
     */
    public function show(Slide $slide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit(Slide $slide)
    {
        return view('admin.slide_edit',compact('slide'));
    }

    /**
     *
     */
    public function update(SlideRequest $request, Slide $slide)
    {

        $slide->update($request->all());
        return redirect(route('admin.slide.index'))->with('success','更新成功！');
    }

    /**
     * Remove the specified resource from storage
     */
    public function destroy(Slide $slide)
    {
        $slide->delete();
        return back()->with('success','删除成功！');
    }
}
