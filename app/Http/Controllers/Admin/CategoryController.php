<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use DemeterChain\C;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestValueResolver;

class CategoryController extends Controller
{
    /**
     *  显示栏目的方法
     */
    public function index()
    {
       $categories = Category::paginate(10);

        return view('admin.category_index', compact('categories'));
    }

    /**
     *  添加栏目的方法
     */
    public function create(Category $category)
    {
        $categories = $category->getTreeCategory();

        return view('admin.category_create', compact('categories'));
    }

    /**
     *  保存栏目的方法
     */
    public function store(CategoryRequest $request)
    {
        Category::create($request->all());

        return back()->with('success', '栏目添加成功！');
    }

    /**
     * 具体信息展示方法
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * 编辑栏目方法
     */
    public function edit(Category $category)
    {
        $categories = $category->getTreeCategory();

        return view('admin.category_edit',compact('category','categories'));
    }

    /**
     * 更新栏目的方法
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->update($request->all());
        return redirect(route('admin.category.index'))->with('success','栏目更新成功！');
    }

    /**
     * 删除栏目的方法
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect(route('admin.category.index'))->with('success','删除成功！');
    }
}
