<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     *  展示文章的方法
     */
    public function index()
    {
        $articles = Article::paginate(6);
        return view('admin.article_index',compact('articles'));
    }

    /**
     * 添加文章的方法
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        // 获取分类的树状结构
        $categories = $category->getTreeCategory();
        return view('admin.article_create',compact('categories'));
    }

    /**
     * 保存方法
     */
    public function store(ArticleRequest $request)
    {
        Article::create($request->all());
        return redirect(route('admin.article.index'))->with('success','新闻添加成功！');
    }

    public function show(Article $article)
    {
        //
    }

    /**
     * 编辑方法
     */
    public function edit(Article $article,Category $category)
    {
        // 这里因为还会用到 所属分类，所以要仍然要获取到树状结构的分类信息
        $categories = $category->getTreeCategory();
        return view('admin.article_edit',compact('categories','article'));
    }

    /**
     *  更新文章内容的方法
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $article->update($request->all());
        return redirect(route('admin.article.index'))->with('success','更新成功！');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return back()->with('success','删除成功！');
    }
}
