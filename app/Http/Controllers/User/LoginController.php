<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    // 加载登录页面的方法
    public function login(){
        return view('user.login');
    }

    // 保存登录数据的方法
    public function store(Request $request){
        // attemp验证方法，会拿着表单提交过来的数据跟后台数据做比较，如果两者匹配，就通过。返回布尔值
       $status = \Auth::attempt([
           'email' =>$request->get('email'),
           'password' =>$request->get('password')
       ]);
       if($status){
           return redirect(route('admin.index'))->with('success','登录成功！');
       }else{
           return back()->with('error','用户名或密码错误');
       }
    }
    // 退出登录方法
    public function logout(){
        auth()->logout();
        return redirect(route('user.login'))->with('success','退出成功！');
    }
}
